<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Shoring Engineers - the best start for a solid foundation</title>
<link href="../css/main.css" rel="stylesheet" type="text/css" />
<style type="text/css">
<!--
#topMenu {
	float: left;
	height: 38px;
	width: 980px;
}
body {
	margin-top: 0px;
	margin-bottom: 0px;
}
a:link {
	color: #4e5050;
	text-decoration: none;
}
a:visited {
	text-decoration: none;
	color: #4e5050;
}
a:hover {
	text-decoration: underline;
	color: #4e5050;
}
a:active {
	text-decoration: none;
	color: #4e5050;
}
-->
</style>
<script src="../Scripts/swfobject_modified.js" type="text/javascript"></script>
<script type="text/javascript">
<!--
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
//-->
</script>
</head>

<body onload="MM_preloadImages('../images/aboutUs_on.jpg','../images/ProjectGallery_on.jpg','../images/resources_on.jpg','../images/expertise_on.jpg','../images/safety_on.jpg','../images/News_events_on.jpg','../images/faq_on.jpg','../images/contact_on.jpg','../images/home_on.jpg')">
<div id="wrapping">
  <div id="topNav"><a href="../index.php" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Home','','../images/home_on.jpg',1)"><img src="../images/homeBtn_off.jpg" alt="Home" name="Home" width="100" height="38" border="0" id="Home" /></a><a href="../about_us.php" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('About Us','','../images/AboutUsBtn_on.jpg',1)"><img src="../images/AboutUsBtn_off.jpg" alt="About Us" name="About Us" width="109" height="38" border="0" id="About Us" /></a><a href="../projects.php" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Project Gallery','','../images/ProjectGalleryBtn_on.jpg',1)"><img src="../images/ProjectGalleryBtn_on.jpg" alt="Project Gallery" name="Project Gallery" width="168" height="38" border="0" id="Project Gallery" /></a><a href="../resources.php" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Resources','','../images/resourcesBTn_on.jpg',1)"><img src="../images/resourcesBtn_off.jpg" alt="Resources" name="Resources" width="124" height="38" border="0" id="Resources" /></a><a href="../expertise.php" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Expertise','','../images/expertiseBtn_on.jpg',1)"><img src="../images/expertiseBtn_off.jpg" alt="Expertise" name="Expertise" width="117" height="38" border="0" id="Expertise" /></a><a href="../safety.php" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Safety','','../images/safetyBtn_on.jpg',1)"><img src="../images/safetyBtn_off.jpg" alt="Safety" name="Safety" width="93" height="38" border="0" id="Safety" /></a><a href="../news.php" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('News and Events','','../images/newsBtn_on.jpg',1)"><img src="../images/newsBtn_off.jpg" alt="News and Events" name="News and Events" width="152" height="38" border="0" id="News and Events" /></a><a href="../contact.php" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Contact','','../images/contactBtn_on.jpg',1)"><img src="../images/contactBtn_off.jpg" alt="Contact" name="Contact" width="117" height="38" border="0" id="Contact" /></a></div>
  <div id="leftCol">
    <div id="logo2"><img src="../images/logo_sm.jpg" width="186" height="155" /></div>
    <div id="ContactBox">Contact us today for a solid bid for your next project, or more information.<br />
       <br />
       Call <span style="font-size: 16px; font-weight: bold;">(562) 944-9331</span><br />
       or request more information <br />
       by clicking below.
       
    </div>
   <div id="InfoBtn"><a href="../contact.php"><img src="../images/RequestInfo_btn.jpg" width="186" height="46" border="0" /></a></div>
  
   <div id="leftBottomInfoBox"><strong>Shoring Engineers</strong><br />
     12645 Clark St. <br />
     Santa Fe Springs, CA 90670<br />
     Phone: (562) 944-9331 <br />
     Fax: (562) 941-8098<br />
     Email: <a href="mailto:info@shoringengineers.com">info@shoringengineers.com</a><br />
     CA Lic. #245416-A <br />
     AZ Lic. #ROC13450<br />
     NV Lic. #0042707-A <br />
     OR Lic. #188382
     <br />
     UT Lic. #96-312403- 5501-E100 <br />
    WA Lic. #2610
    </div>
  </div>
<div id="topImage">
    <object id="FlashID" classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" width="795" height="594">
      <param name="movie" value="simpleviewer.swf" />
      <param name="quality" value="high" />
      <param name="wmode" value="opaque" />
      <param name="swfversion" value="9.0.45.0" />
      <!-- This param tag prompts users with Flash Player 6.0 r65 and higher to download the latest version of Flash Player. Delete it if you don’t want users to see the prompt. -->
      <param name="expressinstall" value="../Scripts/expressInstall.swf" />
      <!-- Next object tag is for non-IE browsers. So hide it from IE using IECC. -->
      <!--[if !IE]>-->
      <object type="application/x-shockwave-flash" data="simpleviewer.swf" width="795" height="594">
        <!--<![endif]-->
        <param name="quality" value="high" />
        <param name="wmode" value="opaque" />
        <param name="swfversion" value="9.0.45.0" />
        <param name="expressinstall" value="../Scripts/expressInstall.swf" />
        <!-- The browser displays the following alternative content for users with Flash Player 6.0 and older. -->
        <div>
          <h4>Content on this page requires a newer version of Adobe Flash Player.</h4>
          <p><a href="http://www.adobe.com/go/getflashplayer"><img src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif" alt="Get Adobe Flash player" width="112" height="33" /></a></p>
        </div>
        <!--[if !IE]>-->
      </object>
      <!--<![endif]-->
    </object>

  </div>
  <?php require_once('../includes/bottomMenu.php'); ?>
<div class="clear"> </div>
</div>
<script type="text/javascript">
<!--
swfobject.registerObject("FlashID");
//-->
</script>
</body>
</html>
