<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Structural Shotcrete Systems</title>
<link href="css/main.css" rel="stylesheet" type="text/css" />
<style type="text/css">
<!--
body {
  margin-top: 0px;
}
a:link {
  color: #000;
  text-decoration: none;
}
a:visited {
  text-decoration: none;
  color: #666;
}
a:hover {
  text-decoration: underline;
  color: #666;
}
a:active {
  text-decoration: none;
  color: #000;
}
-->
</style>

<script type="text/javascript">
<?php 
session_start();

if( isset($_POST['submit'])) {
   if( $_SESSION['security_code'] == $_POST['security_code'] && !empty($_SESSION['security_code'] ) ) {
		// Insert you code for processing the form here, e.g emailing the submission, entering it into a database. 
		echo 'Thank you. Your message said "'.$_POST['message'].'"';
		unset($_SESSION['security_code']);
   } else {
		// Insert your code for showing an error message here
		echo 'Sorry, you have provided an invalid security code';
   }
} else {
?>
<!--
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
//-->
</script></head>

<body>
<div id="wrapping">
<div id="topbar"><img src="images/SSS_top242.jpg" width="242" height="19" /></div>
<div id="topMenualt"><img src="images/topmenu_alt.jpg" width="980" height="85" border="0" usemap="#Map" />
    <map name="Map" id="Map">
      <area shape="rect" coords="804,14,888,46" href="company.html" />
      <area shape="rect" coords="678,15,786,50" href="news.html" />
<area shape="rect" coords="253,15,332,50" href="shotcrete.html" />
      <area shape="rect" coords="347,14,497,50" href="services.html" />
      <area shape="rect" coords="507,15,581,48" href="projects.html" />
      <area shape="rect" coords="896,14,973,48" href="contact.php" />
      <area shape="rect" coords="598,14,660,49" href="gallery/gallery.html" />
      <area shape="rect" coords="20,10,243,76" href="index.html" />
    </map>
  </div>
<div id="mainContent2">
  <div id="sideMenu"><img src="images/sss.jpg" width="130" height="85" /><br />
    <br />
    <b>Structural Shotcrete Systems, Inc.</b><br />
    12645 Clark St. <br />
    Santa Fe Springs, CA 90670<br />
    P: (562) 941-9916<br />
    F: (562) 941-8098<br />
    Email: (<a href="mailto:info@structuralshotcrete.com">click here</a>)<br />
    <br />
    <br />
    <a href="index.html">Home </a></div>
  <div id="textArea" class="cms-editable"><img id="e896c1" src="images/contact.png" alt="" width="471" height="99" /><br /> <br />
Please use the form below to send your comments, questions or feedback <br /> directly to us. No solicitations please!<br />
<br />
<form action="fileform/submit.php" method="post" enctype="multipart/form-data">
  <table width="470" border="0" align="center" cellpadding="4" cellspacing="0">
        <tr>
          <td colspan="2" bgcolor="#6B938A" style="color: #FFF; font-weight: bold;"><div align="center">CONTACT INFORMATION</div></td>
        </tr>
        <tr>
          <td><br /></td>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td width="101"><label for="author">
            <div align="left">Name:</div>
            </label></td>
          <td width="457"><input name="contact_name" type="text" id="contact_name" size="50" /></td>
        </tr>
        <tr>
          <td><div align="left">Company:</div></td>
          <td><input name="company" type="text" id="company" size="50" /></td>
        </tr>
        <tr>
          <td><div align="left">Email Address:</div></td>
          <td><input name="contact_email" type="text" id="conpany" size="50" /></td>
        </tr>
        <tr>
          <td><label for="phone2">
            <div align="left">Phone Number:</div>
            </label></td>
          <td><input name="phone" id="phone" type="text" /></td>
        </tr>
        <tr>
          <td><div align="left">Street Address:</div></td>
          <td><input name="address" type="text" id="conpany2" size="50" /></td>
        </tr>
        <tr>
          <td><div align="left">City/ZIP:</div></td>
          <td><input name="city" type="text" id="conpany3" size="50" /></td>
        </tr>
        <tr>
          <td valign="top"><div align="left">Describe project or challenge:</div></td>
          <td><textarea name="contact_text" id="contact_text" cols="50" rows="10"></textarea></td>
        </tr>
        <tr>
          <td>Request bid by (date/time)</td>
          <td><input name="request_service" type="text" id="conpany4" size="50" /></td>
        </tr>
        <tr>
          <td valign="top"><div align="left">Includes:</div></td>
          <td><label>File 1:
                    <input type="file" name="images[]" id="file1" />
                  </label>
                </p>
                <p>
                  <label>File 2:
                    <input type="file" name="images[]" id="file2" />
                  </label>
                </p>
                <p>
                  <label>File 3:
                    <input type="file" name="images[]" id="file3" />
          </label></td>
        </tr>
        <tr>
          <td></td>
          <td>

<img src="CaptchaSecurityImages.php?width=100&height=40&characters=5" alt="captcha" />
<input id="security_code" name="security_code" type="text" /></td>
        </tr>
        <tr>
          <td></td>
          <td><input type="checkbox" name="email_copy" id="contact_email_copy" value="1"  />
          <label for="contact_email_copy"> E-mail a copy of this message to your own address. </label></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td><input name="submit" type="submit" id="submit" value="Submit" />
            <br />
            <br />
            <br /></td>
        </tr>
  </table></form>
<p><br /> <br /></p>
</div>
  <div id="rightCol"><img src="images/committment.jpg" width="243" height="536" /></div>
</div>
 <div id="bottomNav">
    <div align="center">© Copyright Structural Shotcrete Systems 2010. All RIghts Reserved. CA License # 579272 A<br />
      A WollnerStudios site
    </div>
  </div>
  </div>
</body>
</html>
