<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Structural Shotcrete Systems</title>
<link href="css/main.css" rel="stylesheet" type="text/css" />
<style type="text/css">
<!--
body {
	background-image: url();
	background-repeat: repeat-x;
	margin-top: 0px;
}
-->
</style>
</head>

<body>
<div id="wrapping">
  <div id="topbar"><img src="images/SSS_top242.jpg" width="242" height="19" /></div>
  <div id="topMenualt"><img src="images/topmenu_alt.jpg" width="980" height="85" border="0" usemap="#Map" />
    <map name="Map" id="Map">
      <area shape="rect" coords="738,16,861,52" href="company.html" />
      <area shape="rect" coords="23,7,257,79" href="index.html" />
<area shape="rect" coords="270,15,366,52" href="shotcrete.html" />
      <area shape="rect" coords="379,15,535,53" href="services.html" />
      <area shape="rect" coords="547,16,643,50" href="projects.html" />
      <area shape="rect" coords="872,12,968,50" href="contact.html" />
      <area shape="rect" coords="650,15,720,50" href="gallery.html" />
    </map>
  </div>
  <div id="fpo"> <img src="images/galleryFPO.jpg" width="979" height="489" />
    <map name="Map2" id="Map2">
      <area shape="rect" coords="15,90,123,116" href="shotcrete.html" />
      <area shape="rect" coords="344,85,444,109" href="management.html" />
      <area shape="rect" coords="676,87,763,118" href="company.html" />
    </map>
  </div>
  <div id="bottomNav">
    <div align="center">© Copyright Structural Shotcrete Systems 2010. All RIghts Reserved. CA License # 579272 A<br />
      A WollnerStudios site
    </div>
  </div>
</div>
</body>
</html>
